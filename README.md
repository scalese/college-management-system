# An Example Backend Project with Spring Boot

The project idea based on a (little) college-management-system.

## Dependency

- Docker
- Maven
- java-jdk-8

## Tools, that I use

- IDE: Intellij
- DB Client: Intellij database plugin
- HTTP Tool: Curl

---

## REST Paths

| Method |        Path        |      Description       |
| ------ | :----------------: | :--------------------: |
| GET    |   /api/students    | get a list of students |
| GET    | /api/students/{id} |  get a student by id   |
| POST   |   /api/students    |    create a student    |
| PUT    |   /api/students    |    update a student    |
| DELTE  | /api/students/{id} | delete a student by id |

### Path in general

/api/entityname/

### Entity names

- students
- courses
- instructors

## Software Architecture Overview

![Software Architecture Overview](software-architecture.png)

**Note:** This pattern will also be used for the course and instructor entity.

## Database Overview

### Relationship Model

![Database Relationship Model](db-schema-college-management-system.png)

### Table Schema

![Table Schema](table-schema.png)

## Launch the project

### Create mariadb database

The docker-compose command pulls an mariadb image and start the database server on port _localhost:3306_.

1. ```bash
   cd workspace/college-management-system/
   docker-compose up
   ```

This make command exec an install shell script inside the container. First, the script creats a database (db_college_management_system) and add an admin user with password. Furthermore, the script creates the tables for the database. And last, data from csv files are imported.

2. ```bash
   make local-dev
   ```

3. (Optional) You can connect your DB Client and have a look in the database. Credentials are:

- username: `admin`
- password: `password`
- db-adress: `jdbc:mariadb://localhost:3306/db_college_management_system`

### Launch the project

1. Import the `workspace/college-management-system/pom.xml` as Maven project in your IDE and run the application.
2. Create some requests in Postman or curl ( `http://localhost:8080/api/instructors` ).

### Example with Curl

```bash

# List all courses from a student

curl --request GET \
--url http://localhost:8080/api/students/1/courses

```

## Further work

- Implement tests for all services.
- Implement a Front-end in JSP, React or Thymeleaf.
