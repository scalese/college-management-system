CREATE DATABASE IF NOT EXISTS `db_college_management_system`;

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
GRANT USAGE ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON `db_college_management_system`.* TO 'admin';
FLUSH PRIVILEGES;
