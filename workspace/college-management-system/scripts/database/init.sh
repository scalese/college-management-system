#!/bin/bash

# create data base with admin user
mysql -u root -p"${MYSQL_ROOT_PASSWORD}" < create-db.sql

# create tables
mysql -u root -p"${MYSQL_ROOT_PASSWORD}" "${MYSQ_DATABASE}" < create-tables.sql

# import instructors
mysqlimport -u root -p"${MYSQL_ROOT_PASSWORD}" "${MYSQ_DATABASE}" \
  --ignore-lines=1 --fields-terminated-by="," --fields-enclosed-by="\n" < ./datasets/instructor.csv

# import courses
mysqlimport -u root -p"${MYSQL_ROOT_PASSWORD}" "${MYSQ_DATABASE}" \
  --ignore-lines=1 --fields-terminated-by="," --fields-enclosed-by="\n" < ./datasets/course.csv

# import students
mysqlimport -u root -p"${MYSQL_ROOT_PASSWORD}" "${MYSQ_DATABASE}" \
  --ignore-lines=1 --fields-terminated-by="," --fields-enclosed-by="\n" < ./datasets/student.csv

# import student_course
mysqlimport -u root -p"${MYSQL_ROOT_PASSWORD}" "${MYSQ_DATABASE}" \
  --ignore-lines=1 --fields-terminated-by="," --fields-enclosed-by="\n" < ./datasets/student_course.csv

exit 0;